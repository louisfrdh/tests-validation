﻿// See https://aka.ms/new-console-template for more information
using BusinessDomain;
using ConsoleApp;

Console.WriteLine("Hello, World!");
var insurance = new InsuranceBuilder()
                .ForItEngineer()
                .ForSmoker()
                .ForCardiac()
                .Insurance();
CalculPrinter.PrintCalcul(175000, 1.27, insurance, 25);

insurance = new InsuranceBuilder()
    .ForFighterPilot()
    .ForSportsman()
    .Insurance();
CalculPrinter.PrintCalcul(200000, 0.73, insurance, 15);

Console.ReadLine();