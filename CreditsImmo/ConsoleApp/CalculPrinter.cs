﻿using BusinessDomain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp
{
    public class CalculPrinter
    {
        public static void PrintCalcul(uint borrowedAmount, double annualRate, Insurance insurance, uint loanDurationInYears)
        {
            BorrowedCapital borrowedCapital = new BorrowedCapital(borrowedAmount);
            NominalRate nominalRate = new NominalRate(annualRate);
            LoanDuration loanDuration = new LoanDuration(loanDurationInYears);

            MonthlyPayment monthlyPayment = new MonthlyPayment(borrowedCapital, nominalRate, insurance, loanDuration);

            double totalInterests = loanDuration.Months * monthlyPayment.InterestAmount - borrowedAmount;
            double totalAssurance = loanDuration.Months * monthlyPayment.InsuranceAmount;
            double repaidCapitalAfter10Years = borrowedAmount - (monthlyPayment.InterestAmount * 10 * 12);

            Console.WriteLine($"Pour {borrowedAmount}e empruntés sur {loanDurationInYears} ans avec un taux de {annualRate}% :");
            Console.WriteLine($"Mensualité globale : {monthlyPayment.TotalAmount}e/mois");
            Console.WriteLine($"Mensualité assurance : {monthlyPayment.InsuranceAmount}e/mois ({insurance.Rate}%)");
            Console.WriteLine($"Total des intérêts : {monthlyPayment.InterestAmount}e/mois soit {totalInterests}e");
            Console.WriteLine($"Total de l'assurance : {totalAssurance}e");
            Console.WriteLine($"Total capital remboursé après 3 ans : {repaidCapitalAfter10Years}e\n");
        }
    }
}
