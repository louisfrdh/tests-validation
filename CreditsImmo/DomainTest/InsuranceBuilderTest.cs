﻿using BusinessDomain;
using System;
using Xunit;

namespace DomainTest
{
    public class InsuranceBuilderTest
    {
        private IInsuranceBuilder insuranceBuilder = new InsuranceBuilder();
        private Insurance insurance => insuranceBuilder.Insurance();

        [Fact]
        [Trait("InsuranceBuilder", "Sportsman")]
        public void ForSportsmanOnDefaultInsurance_OK()
        {
            insuranceBuilder.ForSportsman();
            Assert.Equal(insurance.Rate, Insurance.DEFAULT_RATE + InsuranceBuilder.SPORTSMAN_RATE);
        }

        [Fact]
        [Trait("InsuranceBuilder", "Sportsman")]
        public void ForSportsmanBelowMinimumRate_ShouldSetMinimumRate()
        {
            int nbReductionsToBeBelowMinimum = (int)((Insurance.DEFAULT_RATE - Insurance.MINIMUM_RATE)
                                                    / Math.Abs(InsuranceBuilder.SPORTSMAN_RATE)) + 1;
            for (int i = 0; i <= nbReductionsToBeBelowMinimum; i++)
                insuranceBuilder.ForSportsman();
            Assert.Equal(insurance.Rate, Insurance.MINIMUM_RATE);
        }

        [Fact]
        [Trait("InsuranceBuilder", "ItEngineer")]
        public void ForItEngineerOnDefaultInsurance_OK()
        {
            insuranceBuilder.ForItEngineer();
            Assert.Equal(insurance.Rate, Insurance.DEFAULT_RATE + InsuranceBuilder.IT_ENGINEER_RATE);
        }

        [Fact]
        [Trait("InsuranceBuilder", "ItEngineer")]
        public void ForItEngineerBelowMinimumRate_ShouldSetMinimumRate()
        {
            int nbReductionsToBeBelowMinimum = (int)((Insurance.DEFAULT_RATE - Insurance.MINIMUM_RATE)
                                                    / Math.Abs(InsuranceBuilder.IT_ENGINEER_RATE)) + 1;
            for (int i = 0; i <= nbReductionsToBeBelowMinimum; i++)
                insuranceBuilder.ForItEngineer();
            Assert.Equal(insurance.Rate, Insurance.MINIMUM_RATE);
        }

        [Fact]
        [Trait("InsuranceBuilder", "Smoker")]
        public void ForSmokerOnDefaultInsurance_OK()
        {
            insuranceBuilder.ForSmoker();
            Assert.Equal(insurance.Rate, Insurance.DEFAULT_RATE + InsuranceBuilder.FIGHTER_PILOT_RATE);
        }

        [Fact]
        [Trait("InsuranceBuilder", "Smoker")]
        public void ForSmokerAboveMaximumRate_ShouldSetMaximumRate()
        {
            int nbIncreasesToBeAboveMaximum = (int)((Insurance.MAXIMUM_RATE - Insurance.DEFAULT_RATE)
                / Math.Abs(InsuranceBuilder.SMOKER_RATE)) + 1;
            for (int i = 0; i <= nbIncreasesToBeAboveMaximum; i++)
                insuranceBuilder.ForSmoker();
            Assert.Equal(insurance.Rate, Insurance.MAXIMUM_RATE);
        }

        [Fact]
        [Trait("InsuranceBuilder", "Cardiac")]
        public void ForCardiacOnDefaultInsurance_OK()
        {
            insuranceBuilder.ForCardiac();
            Assert.Equal(insurance.Rate, Insurance.DEFAULT_RATE + InsuranceBuilder.CARDIAC_RATE);
        }

        [Fact]
        [Trait("InsuranceBuilder", "Cardiac")]
        public void ForCardiacAboveMaximumRate_ShouldSetMaximumRate()
        {
            int nbIncreasesToBeAboveMaximum = (int)((Insurance.MAXIMUM_RATE - Insurance.DEFAULT_RATE)
                / Math.Abs(InsuranceBuilder.CARDIAC_RATE)) + 1;
            for (int i = 0; i <= nbIncreasesToBeAboveMaximum; i++)
                insuranceBuilder.ForCardiac();
            Assert.Equal(insurance.Rate, Insurance.MAXIMUM_RATE);
        }

        [Fact]
        [Trait("InsuranceBuilder", "FighterPilot")]
        public void ForFighterPilotOnDefaultInsurance_OK()
        {
            insuranceBuilder.ForFighterPilot();
            Assert.Equal(insurance.Rate, Insurance.DEFAULT_RATE + InsuranceBuilder.FIGHTER_PILOT_RATE);
        }

        [Fact]
        [Trait("InsuranceBuilder", "FighterPilot")]
        public void ForFighterPilotAboveMaximumRate_ShouldSetMaximumRate()
        {
            int nbIncreasesToBeAboveMaximum = (int)((Insurance.MAXIMUM_RATE - Insurance.DEFAULT_RATE)
                / Math.Abs(InsuranceBuilder.FIGHTER_PILOT_RATE)) + 1;
            for (int i = 0; i <= nbIncreasesToBeAboveMaximum; i++)
                insuranceBuilder.ForFighterPilot();
            Assert.Equal(insurance.Rate, Insurance.MAXIMUM_RATE);
        }
    }
}
