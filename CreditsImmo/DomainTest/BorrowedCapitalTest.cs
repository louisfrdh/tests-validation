﻿using BusinessDomain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace DomainTest
{
    public class BorrowedCapitalTest
    {
        private const uint BELOW_MINIMUM_AMOUNT = BorrowedCapital.MINIMUM_AMOUNT - 1;
        private BorrowedCapital borrowedCapital;

        [Theory]
        [InlineData(BorrowedCapital.MINIMUM_AMOUNT)]
        [InlineData(BorrowedCapital.MINIMUM_AMOUNT + 1)]
        [InlineData(BorrowedCapital.MINIMUM_AMOUNT + 368516)]
        [Trait("BorrowedCapital", "Set")]
        public void SetAmount_OK(uint amount)
        {
            borrowedCapital = new BorrowedCapital(amount);
            Assert.Equal(amount, borrowedCapital.Amount);
        }

        [Fact]
        [Trait("BorrowedCapital", "Set")]
        public void SetBelowMinimumAmount_ShouldThrowError()
        {
            Assert.Throws<Exception>(() => borrowedCapital = new BorrowedCapital(BELOW_MINIMUM_AMOUNT));
        }
    }
}
