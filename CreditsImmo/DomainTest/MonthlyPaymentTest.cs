﻿using BusinessDomain;
using Xunit;

namespace DomainTest
{
    public class MonthlyPaymentTest
    {
        private MonthlyPayment monthlyPayment;

        [Theory]
        [InlineData(175000, 1.27, 25, 681)]
        [InlineData(200000, 0.73, 15, 1173)]
        public void CalculateMonthlyInterests_OK(uint borrowedAmount, double annualRate, uint loanDurationInYears, int result)
        {
            BorrowedCapital borrowedCapital = new BorrowedCapital(borrowedAmount);
            NominalRate nominalRate = new NominalRate(annualRate);
            LoanDuration loanDuration = new LoanDuration(loanDurationInYears);
            Insurance insurance = new Insurance();
            monthlyPayment = new MonthlyPayment(borrowedCapital, nominalRate, insurance, loanDuration);
            Assert.Equal(result, (int)monthlyPayment.InterestAmount);
        }

        [Fact]
        public void CalculateMonthlyAssurance_OK()
        {
            BorrowedCapital borrowedCapital = new BorrowedCapital(175000);
            NominalRate nominalRate = new NominalRate(2);
            LoanDuration loanDuration = new LoanDuration(LoanDuration.MAXIMUM_YEARS);
            Insurance insurance = new InsuranceBuilder()
                .ForItEngineer()
                .ForSmoker()
                .ForCardiac()
                .Insurance();
            monthlyPayment = new MonthlyPayment(borrowedCapital, nominalRate, insurance, loanDuration);
            Assert.Equal(102, (int)monthlyPayment.InsuranceAmount);
        }
    }
}
