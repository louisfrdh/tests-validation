﻿using BusinessDomain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace DomainTest
{
    public class LoanDurationTest
    {
        private const uint BELOW_MINIMUM_YEARS = LoanDuration.MINIMUM_YEARS - 1;
        private const uint ABOVE_MINIMUM_YEARS = LoanDuration.MINIMUM_YEARS + 1;
        private const uint BELOW_MAXIMUM_YEARS = LoanDuration.MAXIMUM_YEARS - 1;
        private const uint ABOVE_MAXIMUM_YEARS = LoanDuration.MAXIMUM_YEARS + 1;
        private LoanDuration loanDuration;

        [Theory]
        [InlineData(LoanDuration.MINIMUM_YEARS)]
        [InlineData(LoanDuration.MAXIMUM_YEARS)]
        [InlineData(BELOW_MAXIMUM_YEARS)]
        [InlineData(ABOVE_MINIMUM_YEARS)]
        [Trait("LoanDuration", "Set")]
        public void SetDuration_OK(uint duration)
        {
            loanDuration = new LoanDuration(duration);
        }

        [Theory]
        [InlineData(BELOW_MINIMUM_YEARS)]
        [InlineData(0)]
        [Trait("LoanDuration", "Set")]
        public void SetDurationBelowMinimum_ShouldThrowException(uint duration)
        {
            Assert.Throws<Exception>(() => loanDuration = new LoanDuration(duration));
        }

        [Theory]
        [InlineData(ABOVE_MAXIMUM_YEARS)]
        [InlineData(ABOVE_MAXIMUM_YEARS + 17)]
        [Trait("LoanDuration", "Set")]
        public void SetDurationAboveMaximum_ShouldThrowException(uint duration)
        {
            Assert.Throws<Exception>(() => loanDuration = new LoanDuration(duration));
        }
    }
}
