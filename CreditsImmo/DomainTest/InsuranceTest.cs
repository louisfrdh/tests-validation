﻿using BusinessDomain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace DomainTest
{
    public class InsuranceTest
    {
        private const double BELOW_MINIMUM_RATE = Insurance.MINIMUM_RATE - 0.01;
        private const double ABOVE_MINIMUM_RATE = Insurance.MINIMUM_RATE + 0.01;
        private const double BELOW_MAXIMUM_RATE = Insurance.MAXIMUM_RATE - 0.01;
        private const double ABOVE_MAXIMUM_RATE = Insurance.MAXIMUM_RATE + 0.01;
        private Insurance insurance = new Insurance();

        [Theory]
        [InlineData(Insurance.MINIMUM_RATE)]
        [InlineData(Insurance.MAXIMUM_RATE)]
        [InlineData(ABOVE_MINIMUM_RATE)]
        [InlineData(BELOW_MAXIMUM_RATE)]
        [Trait("Insurance", "Set")]
        public void SetRate_OK(double rate)
        {
            insurance.Rate = rate;
            Assert.Equal(rate, insurance.Rate);
        }

        [Theory]
        [InlineData(BELOW_MINIMUM_RATE)]
        [InlineData(0)]
        [Trait("Insurance", "Set")]
        public void SetRateBelowMinimum_ShouldThrowException(double rate)
        {
            Assert.Throws<Exception>(() => insurance.Rate = rate);
        }

        [Theory]
        [InlineData(ABOVE_MAXIMUM_RATE)]
        [InlineData(1)]
        [Trait("Insurance", "Set")]
        public void SetRateAboveMaximum_ShouldThrowException(double rate)
        {
            Assert.Throws<Exception>(() => insurance.Rate = rate);
        }
    }
}
