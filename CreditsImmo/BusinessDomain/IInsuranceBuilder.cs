﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessDomain
{
    public interface IInsuranceBuilder
    {
        Insurance Insurance();
        InsuranceBuilder ForSportsman();
        InsuranceBuilder ForSmoker();
        InsuranceBuilder ForCardiac();
        InsuranceBuilder ForItEngineer();
        InsuranceBuilder ForFighterPilot();

    }
}
