﻿using System;

namespace BusinessDomain
{
    public class BorrowedCapital
    {
        public const uint MINIMUM_AMOUNT = 50000;

        private uint amount;
        public uint Amount
        {
            get { return amount; }
            set
            { 
                if(CorrectBorrowedAmount(value))
                    amount = value;
                else throw new Exception($"The amount must be greater than {MINIMUM_AMOUNT}.");
            }
        }

        public BorrowedCapital(uint amount)
        {
            Amount = amount;
        }

        private bool CorrectBorrowedAmount(uint amount) => amount >= MINIMUM_AMOUNT;
    }
}