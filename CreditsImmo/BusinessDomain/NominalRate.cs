﻿namespace BusinessDomain
{
    public class NominalRate
    {
        public double AnnualRate;

        public NominalRate(double annualRate)
        {
            AnnualRate = annualRate;
        }
    }
}