﻿using System;

namespace BusinessDomain
{
    public class LoanDuration
    {
        public const uint MINIMUM_YEARS = 9;
        public const uint MAXIMUM_YEARS = 25;

        private uint years;
        public uint Years
        {
            get { return years; }
            set
            {
                if (CorrectYearsDuration(value))
                    years = value;
                else
                    throw new Exception($"The duration must be between {MINIMUM_YEARS} and {MAXIMUM_YEARS}.");
            }
        }

        public uint Months => Years * 12;

        public LoanDuration(uint durationInYears)
        {
            Years = durationInYears;
        }

        private bool CorrectYearsDuration(uint years) => years <= MAXIMUM_YEARS && years >= MINIMUM_YEARS;
    }
}