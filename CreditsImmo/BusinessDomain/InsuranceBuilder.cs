﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessDomain
{
    public class InsuranceBuilder : IInsuranceBuilder
    {
        public const double SPORTSMAN_RATE = -0.05;
        public const double IT_ENGINEER_RATE = -0.05;
        public const double SMOKER_RATE = 0.15;
        public const double CARDIAC_RATE = 0.3;
        public const double FIGHTER_PILOT_RATE = 0.15;
        private Insurance insurance;

        public InsuranceBuilder()
        {
            insurance = new Insurance();
        }

        public Insurance Insurance() => insurance;

        public InsuranceBuilder ForSportsman()
        {
            try
            {
                insurance.Rate += SPORTSMAN_RATE;
            }
            catch(Exception)
            {
                insurance.SetMinimumRate();
            }
            return this;
        }

        public InsuranceBuilder ForSmoker()
        {
            try
            {
                insurance.Rate += SMOKER_RATE;
            }
            catch (Exception)
            {
                insurance.SetMaximumRate();
            }
            return this;
        }

        public InsuranceBuilder ForCardiac()
        {
            try
            {
                insurance.Rate += CARDIAC_RATE;
            }
            catch (Exception)
            {
                insurance.SetMaximumRate();
            }
            return this;
        }

        public InsuranceBuilder ForItEngineer()
        {
            try
            {
                insurance.Rate += IT_ENGINEER_RATE;
            }
            catch (Exception)
            {
                insurance.SetMinimumRate();
            }
            return this;
        }

        public InsuranceBuilder ForFighterPilot()
        {
            try
            {
                insurance.Rate += FIGHTER_PILOT_RATE;
            }
            catch (Exception)
            {
                insurance.SetMaximumRate();
            }
            return this;
        }
    }
}
