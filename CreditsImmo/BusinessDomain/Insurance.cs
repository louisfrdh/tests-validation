﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessDomain
{
    public class Insurance
    {
        public const double MINIMUM_RATE = 0.1;
        public const double MAXIMUM_RATE = 0.9;
        public const double DEFAULT_RATE = 0.3;
        private double rate;

        public double Rate
        {
            get { return rate; }
            set
            { 
                if(CorrectInsuranceRate(value))
                    rate = value;
                else
                    throw new Exception($"The duration must be between {MINIMUM_RATE} and {MAXIMUM_RATE}.");
            }
        }

        public Insurance()
        {
            Rate = DEFAULT_RATE;
        }

        public void SetMinimumRate()
        {
            Rate = MINIMUM_RATE;
        }

        public void SetMaximumRate()
        {
            Rate = MAXIMUM_RATE;
        }

        private bool CorrectInsuranceRate(double rate) => rate >= MINIMUM_RATE && rate <= MAXIMUM_RATE;
    }
}
