﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessDomain
{
    public class MonthlyPayment
    {
        private BorrowedCapital borrowedCapital;
        private NominalRate nominalRate;
        private Insurance insurance;
        private LoanDuration loanDuration;

        public double InterestAmount
        {
            get
            {
                int nbMonthsInYear = 12;
                double annualRatePercentByNbMonthsInYear = nominalRate.AnnualRate / 100 / nbMonthsInYear;
                return (borrowedCapital.Amount * annualRatePercentByNbMonthsInYear) / 
                    (1 - Math.Pow(1 + annualRatePercentByNbMonthsInYear, -loanDuration.Months));
            }
        }

        public double InsuranceAmount => borrowedCapital.Amount * insurance.Rate / 100 / 12;

        public double TotalAmount => InterestAmount + InsuranceAmount;

        public MonthlyPayment(BorrowedCapital borrowedCapital, NominalRate nominalRate, Insurance insurance, LoanDuration loanDuration)
        {
            this.borrowedCapital = borrowedCapital;
            this.nominalRate = nominalRate;
            this.insurance = insurance;
            this.loanDuration = loanDuration;
        }
    }
}
